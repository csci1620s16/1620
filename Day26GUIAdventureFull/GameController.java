import java.util.ArrayList;
import java.util.List;

public class GameController implements IModel{
	
	
	private Location me;
	
	private String lastErrorString = "";
	
	public GameController() {
		
		
		Location start = new Location("Classroom", "You are in what used to be a classroom. All around you computers and sizzling and desks are burnt. There clearly used to be a lot of people in here since the are cellphones and backpacks scattered everywhere. There is a door that leads out of the room to the east.");
		
		Location hall = new Location("Hall", "You stand in a main hallway. Looking at your cellphone's compass, it's clear that the hall runs north and south. In the distance you hear the screams of students. Probably the students who just fled from the classroom. To the west is an open door that leads to a classroom.");
		
		Location southHall = new Location("South Hall", "You stand at the south end of a hallway, which ends in a large pane of glass. Hovering above the parkinglot outside is a large UFO. You feel really jealous that you weren't chosen to represent earth in the intergalactic games. Your resolve to get on the UFO only grows.");
		
		Location northHall = new Location("North Hall", "You stand at the north end of the hallway. When you came to class this hallways led to a large staircase next to a large glass sculpture. Neither of them exist anymore, since they too seemed to be vaporized by the appearance of the UFOs. The screaming of students is louder here. Several of them seem to have climbed down the rubble to the main floor.  You could try to climb down the rubble if you proceed north.");
		
		Location inUFO = new Location("UFO", "You start to climb down the rubble, but as you do, the rubble starts to slide and soon to you are falling uncontrollably. You cut your hands on the remains of the glass sculpture and finally run into one of the now-bare beams that supports the building. The building shakes, trembles, and then slowly begins to collapse. Just as a large chunk of building is about to hit you, a UFO appears and beams into the main cabin. \r\n\r\nYou wake up after what feels like a long time later. You are floating in a large tube full of a thick liquid. In front of your two aliens are talking to each other as they drive the UFO. 'Why did you go back to get that student?' one asks.\r\nThe other shrugs, 'The was a very graceful fall. I liked it.' \r\nJust then a baby alien waddles in. \r\n'Daddy, are we there yet?' \r\n'No, no darling, it will be at least another three hundred years.'");
		
		
		
		start.setEast(hall);
		hall.setWest(start);
		
		hall.setSouth(southHall);
		hall.setNorth(northHall);
		
		southHall.setNorth(hall);
		
		northHall.setSouth(hall);
		
		northHall.setNorth(inUFO);
		
		
		
		
		me = start;
		
		
		boolean useCommandLine = false;
		 
		if(useCommandLine)
		{
		
			java.util.Scanner scanner = new java.util.Scanner(System.in);
			
			while(true)
			{
				System.out.print(me.getDescription());
				for(PhysicalObject po : me.getPhysicalObjects())
				{
					System.out.print(po.getInlineDescription());
				}
				System.out.println();
				System.out.println();
				
				System.out.print(me.getName() + " >");
				
				String response = scanner.next();
				System.out.println();
				
				
				if(response.equals("e"))
				{
					goEast();
				}
				else if(response.equals("n"))
				{
					goNorth();
				}
				else if(response.equals("w"))
				{
					goWest();
				}
				else if(response.equals("s"))
				{
					goSouth();
				}
				else
				{
					useObject(response);
				}
			}
		}
		
	}

	public void useObject(String objectName) {
		boolean foundWhatToDo = false;
		
		for(PhysicalObject po : me.getPhysicalObjects())
		{
			if(po.getName().toLowerCase().equals(objectName))
			{
				foundWhatToDo = true;
			
				if(po instanceof StaticObject)
				{
					Location connection = ((StaticObject)po).getConnection();
					if(connection != null)
						me = connection;
					else
						System.out.println(po.getDescription());
				}
			}
		}
		
		if(!foundWhatToDo)
		{
			System.out.println("I didn't get that. Why don't you try again.");
		}
	}

	public void goNorth() {
		if(me.getNorth() == null)
			System.out.println("Can't go that way.");
		else
		{
			me = me.getNorth();
			updateViews();
		}
		
	}
	
	public void goEast() {
		if(me.getEast() == null)
			System.out.println("Can't go that way.");
		else
		{
			me = me.getEast();
			updateViews();
		}
		
	}
	
	public void goSouth() {
		if(me.getSouth() == null)
			System.out.println("Can't go that way.");
		else
		{
			me = me.getSouth();
			updateViews();
		}
		
	}
	
	public void goWest() {
		if(me.getWest() == null)
			System.out.println("Can't go that way.");
		else
		{
			me = me.getWest();
			updateViews();
		}
		
	}

	
	List<IView> views = new ArrayList<IView>();
	
	@Override
	public void addView(IView inView) {
		views.add(inView);
	}
	
	public void updateViews()
	{
		for(IView view : views)
		{
			view.updated();
		}
	}

	public String getRoomText() {
		return me.getDescription();
	}

	public String getErrorText() {
		return lastErrorString;
	}

}
