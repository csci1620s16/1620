import javax.swing.JFrame;

public class MainFrame extends JFrame implements IView{

	
	public MainFrame() {
		super("GUI Adventure");
		
		Main.game.addView(this);
		
		this.add(new MainPanel());
		
		this.setVisible(true);
		
		
		this.pack();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	@Override
	public void updated() {
		// TODO Auto-generated method stub
		
	}

}
