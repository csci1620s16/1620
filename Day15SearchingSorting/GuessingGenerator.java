import java.util.ArrayList;
import java.util.List;


public abstract class GuessingGenerator {

	protected List<Integer> numbersIveGuessed = new ArrayList<Integer>();
	
	public GuessingGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public abstract int nextGuess();

	public void addGuess(int guess) {
		numbersIveGuessed.add(guess);
		
	}

	public void guessToHigh() {
		// TODO Auto-generated method stub
		
	}

	public void guessToLow() {
		// TODO Auto-generated method stub
		
	}
	
	
}
