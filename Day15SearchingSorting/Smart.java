
public class Smart extends GuessingGenerator {

	int upperBound = 1000000;
	
	int lowerBound = 0;
	
	
	@Override
	public int nextGuess() {
		return (upperBound - lowerBound)/2 + lowerBound;
	}
	
	@Override
	public void guessToHigh() {
		Integer lastGuess = getLastGuess();
		if(lastGuess < upperBound)
		{
			upperBound = lastGuess;
		}
	}

	private Integer getLastGuess() {
		return numbersIveGuessed.get(numbersIveGuessed.size() - 1);
	}
	
	@Override
	public void guessToLow() {
		Integer lastGuess = getLastGuess();
		if(lastGuess > lowerBound)
		{
			lowerBound = lastGuess;
		}
	}

}
