import java.util.Random;


public class Naive extends GuessingGenerator {

	@Override
	public int nextGuess() {
		return new Random().nextInt(1000000);
	}

}
