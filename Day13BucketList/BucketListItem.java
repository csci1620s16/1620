
public class BucketListItem {
	
	private String Description;
	
	private BucketListItem[] subItems;
	
	private int nextItem = 0;
	
	public BucketListItem(String inDescription)
	{
		Description = inDescription;
		subItems = new BucketListItem[4];
	}
	
	@Override
	public String toString() {
		return Description;
	}
	
	public void addBucketListItem(String inDescription)
	{
		subItems[nextItem] = new BucketListItem(inDescription);
		nextItem++;
	}
	
	public BucketListItem getBucketListItem(int index)
	{
		return subItems[index];
	}

}
