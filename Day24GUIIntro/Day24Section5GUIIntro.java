import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;


public class Main {

	public static void main(String[] args) {
		new Main();

	}
	
	public Main() {
		
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		
		JFrame frame = new JFrame("Notepad X-Treme");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		//panel.setBackground(Color.RED);
		
		frame.add(panel);
		
		panel.setPreferredSize(new Dimension(400, 400));
		
		final JTextArea mainArea = new JTextArea(10,10);
		mainArea.setLineWrap(true);
		mainArea.setWrapStyleWord(true);
		
		panel.add(mainArea);
		
		
		
		JMenuBar menuBar = new JMenuBar();
		
		JMenu menuItem1 = new JMenu("File");
		JMenuItem saveMenuItem = new JMenuItem("Save");
		menuItem1.add(saveMenuItem);
		
		
		saveMenuItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String textAreaValue = mainArea.getText();
				
				System.out.println(textAreaValue);
				 List<String> allStrings = new ArrayList<String>();
				 allStrings.add(textAreaValue);
				
				
				try {
					Files.write(Paths.get("./textHolder.txt"), allStrings, Charset.defaultCharset());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		});
		
		menuBar.add(menuItem1);
		
		frame.setJMenuBar(menuBar);
		
		frame.pack();
		
	}

}
