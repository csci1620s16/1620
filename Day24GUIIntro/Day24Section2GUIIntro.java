import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * Load a GUI for the first time.
 * @author unouser
 *
 */
public class Main {

	public static void main(String[] args) {
		new Main();

	}
	
	public Main() {
		
		
		try {
			UIManager.setLookAndFeel(
			        UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		JFrame mainFrame = new JFrame("The Yes Game!");
		mainFrame.setVisible(true);
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(600, 200));
		//panel.setBackground(Color.LIGHT_GRAY);
		
		mainFrame.add(panel);
		
		JLabel startLabel = new JLabel("Do you want to play the game?");
		panel.add(startLabel);
		
		JButton startButton = new JButton("Yes!");
		
		panel.add(startButton);
		
		final JLabel prompt = new JLabel("What is the magic word?");
		prompt.setVisible(false);
		panel.add(prompt);
		
		final JTextField guess = new JTextField("", 10);
		panel.add(guess);
		guess.setVisible(false);
		
		final JLabel resultLabel = new JLabel("");
		panel.add(resultLabel);
		
		
		final JButton submit = new JButton("Submit");
		submit.setVisible(false);
		submit.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String userInput = guess.getText();
				
				if(userInput.equals("YES"))
				{
					resultLabel.setText("You guessed right. You must have cheated.");
				}
				else
				{
					resultLabel.setText("You guessed wrong. You can only win by looking up the solution on Google.");
				}
			}
		});
		
		startButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				prompt.setVisible(true);
				guess.setVisible(true);
				submit.setVisible(true);
				
			}
			
		});
		
		
		panel.add(submit);
		
		
		
		
		mainFrame.pack();
		
		
	}

}
