/**
 * driver to test my dynamic list
 * @author unouser
 *
 */
public class Main {

	/**
	 * Entry point
	 * @param args command line args (ignored)
	 */
	public static void main(String[] args) {
		
		
		List towers = new DynamicList();
		
		towers.add("Skeleton Tower");
		towers.add("Dragon");
		towers.add("Donuts");
		
		for(int i = 0; i < towers.size(); i++)
		{
			System.out.println(towers.get(i));
		}
		
		towers.removeAt(1);
		
		for(int i = 0; i < towers.size(); i++)
		{
			System.out.println(towers.get(i));
		}
		
		towers.add("Lasers");
		
		for(int i = 0; i < towers.size(); i++)
		{
			System.out.println(towers.get(i));
		}
		
		

	}

}
