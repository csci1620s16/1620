/**
 * A list that changes size.
 * @author unouser
 *
 */
public class DynamicList implements List {

	String[] myArray = new String[1];
	int nextInsertion = 0;
	
	/**
	 * Create a dynamic list
	 */
	public DynamicList() {
	}
	
	/**
	 * Add items to our dynamic lists, expanding as necessary
	 */
	@Override
	public void add(String string) {
		if(nextInsertion >= myArray.length)
		{
			///Create a new array
			String[] newArray = new String[myArray.length+1];
			///Copy things over
			for(int i = 0; i < myArray.length; i++)
			{
				newArray[i] = myArray[i];
			}
			///Get rid of the old array
			myArray = newArray;
		}
		myArray[nextInsertion] = string;
		
		nextInsertion++;
		
	}

	/**
	 * return the size of the dynamic list
	 * @return the size of the dynamic list
	 */
	@Override
	public int size() {
		return nextInsertion;
	}

	/**
	 * Get the ith element from my array
	 * @param i It tells me which element I want
	 * @return That element, or null on a bad entry
	 */
	@Override
	public String get(int i) {
		if(i > nextInsertion - 1 || i < 0)
			return null;
		return myArray[i];
		
	}

	/**
	 * remove the ith element of the list.
	 * @param i The element to remove
	 * @return true if we removed something, false otherwise
	 */
	@Override
	public boolean removeAt(int i) {
		
		if(i > nextInsertion - 1 || i < 0)
			return false;
		for(int j = i; j < nextInsertion - 1; j++)
		{
			myArray[j] = myArray[j+1];
		}
		
		nextInsertion--;
		return true;
	}

}
