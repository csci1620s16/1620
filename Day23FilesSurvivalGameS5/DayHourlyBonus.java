
public class DayHourlyBonus extends Bonus {
	
	
	public DayHourlyBonus() {
		providesFood = -4;
		providesWater = -4;
		providesRest = -4;
		providesHeat = -2;
	}

}
