/**
 * The Class Ship.
 */
public class Ship extends SpaceObject{
	
	/** The radius of the ship. */
	public static final float RADIUS = 1;
	
	/**
	 * Instantiates a new ship.
	 */
	public Ship()
	{
		location = new Point(0,0);
	}

}
