import java.util.ArrayList;
import java.util.List;


public class Bacon {

	public static void main(String[] args) {
		System.out.println("Welcome to the six degrees of Kevin Bacon.");
		
		new Bacon();

	}
	
	
	Actor KevinBacon = new Actor("Kevin Bacon");
	Actor TomHanks = new Actor("Tom Hanks");
	Actor MattDamon = new Actor("Matt Damon");
	Actor BenAffleck = new Actor("Ben Affleck");
	Actor HarveyTheKid = new Actor("Harvey the Kid");
	Actor LeonardoDiCaprio = new Actor("Leonardo DiCaprio.");
	Actor HarrisonFord = new Actor("Harrison Ford");
	Actor KateW = new Actor("Kate W.");
	
	List<Actor> whoWeveSeen = new ArrayList<Actor>();
	
	public Bacon()
	{
		actedTogether(KevinBacon, TomHanks);
		actedTogether(TomHanks, MattDamon);
		actedTogether(TomHanks, LeonardoDiCaprio);
		actedTogether(MattDamon, BenAffleck);
		actedTogether(BenAffleck, HarveyTheKid);
		actedTogether(LeonardoDiCaprio, KateW);
		
		
		System.out.println("Let's find the distance");
		int distance = findDistance(KevinBacon, HarrisonFord);
		System.out.println("The closest distance was " + distance);
		
	}
	
	/**
	 * A method that recursively looks for a connection
	 * @param start The actor I'm starting with
	 * @param search The actor I'm loking for
	 * @return The degree separation if one exists, 1000001 otherwise.
	 */

	private int findDistance(Actor start, Actor search) {
		
		/* to prevent infinite loops */
		whoWeveSeen.add(start);
		/* Stopping condition */
		if(start.starredWith.contains(search))
		{
			System.out.println("I found a connection.");
			return 1;
		}
		int minDistance = 100000000;
		/* Body */
		for(Actor actor : start.starredWith)
		{
			if(whoWeveSeen.contains(actor))
			{}
			else
			{
				/* Post-body change */
				int possibleDistance = findDistance(actor, search);
				if(possibleDistance < minDistance)
					minDistance = possibleDistance;
			}
		}
		return minDistance + 1;
		
	}

	
	private void actedTogether(Actor one, Actor two) {
		one.starredWith.add(two);
		two.starredWith.add(one);
		
	}

}
