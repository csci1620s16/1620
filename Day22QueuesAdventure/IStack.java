
public interface IStack<X> {

	boolean isEmpty();
	
	void add(X inType);
	
	X pop();
	
}
