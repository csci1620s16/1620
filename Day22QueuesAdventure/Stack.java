
public class Stack<X> implements IStack<X> {
	
	Node<X> top = null;

	@Override
	public boolean isEmpty() {
		return top == null;
	}

	@Override
	public void add(X inType) {
		
		Node<X> newNode = new Node<X>();
		newNode.data = inType;
		newNode.previousNode = top;
		top = newNode;

	}

	@Override
	public X pop() {
		if(isEmpty())
		{
			
			return null;
		}
		else{
			X toReturn = top.data;
			top = top.previousNode;
			return toReturn;
		}
	}

}
