import java.awt.BorderLayout;

import javax.swing.JPanel;

public class MainPanel extends JPanel implements View{
	
	public MainPanel() {
		super();
		
		Main.model.addView(this);
		
		this.setLayout(new BorderLayout());
		
		this.add(new PlayerView(Main.model.player1), BorderLayout.WEST);
		
		this.add(new PlayerView(Main.model.player2), BorderLayout.EAST);
		
		this.add(new ControlPanel(), BorderLayout.SOUTH);
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
	
	

}
