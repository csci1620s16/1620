import java.util.ArrayList;
import java.util.List;

public class Player {
	
	public List<Hero> heroes = new ArrayList<Hero>();
	
	private boolean turnSkipped = false;
	
	

	public void skipTurn() {
		turnSkipped = true;
		
	}
	
	public void startTurn()
	{
		for(Hero hero : heroes)
		{
			hero.startTurn();
			
			List<Buff> newBuffs = new ArrayList<Buff>();
			
			
			for(Buff buff : hero.buffs)
			{
				buff.turns -= 1;
				if(buff.turns > 0)
				{
					newBuffs.add(buff);
				}
			}
			
			hero.buffs = newBuffs;
			
			
		}
	}
	
	

}
