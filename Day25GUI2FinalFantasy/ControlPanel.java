import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class ControlPanel extends JPanel implements View {
	
	JLabel selectedHero = new JLabel("No hero selected");
	
	JLabel playerTurn = new JLabel("No player selected.");
	
	JButton attackButton = new JButton("Attack");
	
	JButton specialButton = new JButton("Special");
	
	JButton endTurn = new JButton("End Turn");
	
	
	public ControlPanel() {
		super();
		
		Main.model.addView(this);
		
		this.setBackground(Color.RED);
		
		this.add(playerTurn);
		this.add(selectedHero);
		this.add(attackButton);
		this.add(specialButton);
		this.add(endTurn);
		
		endTurn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.model.endTurn();
				
			}
		});
		
		attackButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JPopupMenu popup = new JPopupMenu();
				
				for(Hero hero : Main.model.getOtherPlayer().heroes)
				{
					
					JMenuItem item = new JMenuItem(hero.getClass().getName());
					item.setEnabled(hero.isAlive());
					popup.add(item);
					final Hero thisHero = hero;
					item.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							Main.model.attacks(thisHero);
							
						}
					});
					
				}
				
				popup.show(attackButton, 0,0);
				
			}
		});
		
		specialButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.model.special();
				
			}
		});
		
		
		
	}

	@Override
	public void update() {
		
		attackButton.setEnabled(false);
		specialButton.setEnabled(false);
		
		if(Main.model.selectedHero == null)
		{
			selectedHero.setText("No Hero selected");
			
		}
		else
		{
			selectedHero.setText(Main.model.selectedHero.getClass().getName() + " Selected");
			
			
			if(Main.model.selectedHero.getCanAttack())
				attackButton.setEnabled(true);
			if(Main.model.selectedHero.canUseCooldown())
				specialButton.setEnabled(true);
		}
		
		
		if(Main.model.isPlayerOnesTurn)
			playerTurn.setText("Player 1's turn");
		else
			playerTurn.setText("Player 2s turn");

	}

}
