import java.awt.Color;
import java.awt.Dimension;
import java.util.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class PlayerView extends JPanel implements View {

	Player myPlayer;
	
	List<HeroPanel> heroPanels = new ArrayList<HeroPanel>();
	
	public PlayerView(Player inPlayer) {
		super();
		
		Main.model.addView(this);
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		myPlayer = inPlayer;
		
		this.setPreferredSize(new Dimension(200, 200));
		this.setMinimumSize(new Dimension(200, 200));
		
		for(Hero hero : inPlayer.heroes)
		{
			heroPanels.add(new HeroPanel(hero));
			this.add(heroPanels.get(heroPanels.size() - 1));
		}
	}

	@Override
	public void update() {
		if(
				(myPlayer == Main.model.player1 && Main.model.isPlayerOnesTurn) ||
				(myPlayer == Main.model.player2 && !Main.model.isPlayerOnesTurn))
		{
			this.setBackground(Color.CYAN);
			
		}
		else
		{
			this.setBackground(Color.WHITE);
			
			
		}
				
				

	}

}
